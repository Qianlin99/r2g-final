import mysql.connector
from mysql.connector import Error

host = "127.0.0.1"
user_name = "root"
password = "password"
database = "restaurant"


def sql_retrieve_all(query):
    connection = mysql.connector.connect(
        host=host,
        user=user_name,
        passwd=password,
        database=database
    )
    cursor = connection.cursor()
    try:
        query = cursor.execute(query)
        print("Query successful")
        result = cursor.fetchall()
        print(result)
        if result:
            return result
        else:
            return []
    except Error as err:
        print(f"Error: '{err}'")
        return False
    finally:
        cursor.close()
        connection.close()


def sql_insert(query):
    connection = mysql.connector.connect(
        host=host,
        user=user_name,
        passwd=password,
        database=database,
        autocommit=True
    )
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query successful")
        return True
    except Error as err:
        print(f"Error: '{err}'")
        return False
    finally:
        cursor.close()
        connection.close()


def sql_update(query):
    connection = mysql.connector.connect(
        host=host,
        user=user_name,
        passwd=password,
        database=database,
        autocommit=True
    )
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query successful")
    except Error as err:
        print(f"Error: '{err}'")
    finally:
        cursor.close()
        connection.close()
