from entity.UserProfile import UserProfile

class AdminViewUserProfController:
    @staticmethod
    def displayUserProf():
        return UserProfile.displayUserProf()