from entity.MenuItem import MenuItem

class RestManagerViewMenuItemController:
    @staticmethod
    def displayMenuItem():
        return MenuItem.displayMenuItem()