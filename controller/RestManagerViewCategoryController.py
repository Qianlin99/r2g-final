from entity.Category import Category


class RestManagerViewCategoryController:
    def __init__(self):
        pass
    @staticmethod
    def displayCategory():
        return Category.displayCategory()
