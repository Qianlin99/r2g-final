from entity.UserProfile import UserProfile

class AdminSearchUserProfController:
    @staticmethod
    def displayUserProfInfo(roles):
        return UserProfile.validateSearch(roles)