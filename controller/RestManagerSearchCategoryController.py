from entity.Category import Category


class RestManagerSearchCategoryController:
    def __init__(self):
        pass

    @staticmethod
    def displayCategoryInfo(CategoryName):
        return Category.validateSearch(CategoryName)
