FROM python:3.7.5-slim-buster

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .


CMD gunicorn -b 0.0.0.0:5000 --daemon - "main.app:create_app()"